#!/bin/sh

# Set up the copy of Antora's default ui
mkdir ui
(cd ui; git clone https://gitlab.com/antora/antora-ui-default.git;cd antora-ui-default;git checkout -b custom-ui;npm i)

# Set up the playbook git project

(cd playbook; git init; git add *.yml *.sh; git commit -m "Initial commit of playbooks and helper scripts")

# Set up the site git project

(cd site; git init; git add modules antora.yml; git commit -m "Initial commit of site")
