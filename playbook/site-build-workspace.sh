#!/bin/sh

echo Bundling the ui and building the site from the local filesystem

(cd ../ui/antora-ui-default;gulp bundle)

antora --fetch --clean antora-playbook.yml
