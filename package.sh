#!/bin/sh

# get back to a pristine state

git clean -xdff

rm -rf playbook/.git
rm -rf site/.git
rm -rf ui

# package
tar -czf antora-quickstart.tar.gz setup.sh playbook site
